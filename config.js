const joi = require('joi');

// require and configure dotenv, will load vars in .env in PROCESS.ENV
require('dotenv').config();

// 建立每個變數 joi 驗證規則
const envVarSchema = joi.object().keys({
  NODE_ENV: joi.string().default('development').allow('development', 'production'), // 字串且預設值為development 並只允許兩種參數
  VERSION: joi.string() 
}).unknown().required();

// process.env 撈取 .env 內的變數做 joi 驗證
const { error, value: envVars } = joi.validate(process.env, envVarSchema);

if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

const config = {
  service: envVars.SERVICE,
  stage: envVars.STAGE,
  region: envVars.REGION,
  version: envVars.VERSION,
  env: envVars.NODE_ENV,

  offline: {
    http_port: envVars.OFFLINE_HTTP_PORT,
    dynamodb_port: envVars.OFFLINE_DYNAMODB_PORT,
  },
  
  dynamodb: {
    RCU: 2,
    WCU: 2,
  },

  table: {
    users: envVars.TABLE_USERS,
    nextid: envVars.TABLE_NEXTID
  }
};

module.exports = config;