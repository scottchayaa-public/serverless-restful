'use strict';

const httpErrors = require('http-errors');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const expressValidation = require('express-validation');

const apiError = require('./api-error');
const config = require('./config');

/**
 * Middleware
 */

// CORS
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

/**
 * Router
 */
app.use('/', require('./router'));
app.use('/users', require('./users/router'));

/**
 * Error handler
 */

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(new apiError(404, 0, 'page not found'));
});

// check Error type, and transfer to ApiError 
app.use((err, req, res, next) => {
  if (err instanceof apiError === false) { 
    if (err instanceof expressValidation.ValidationError) {
      if (err.errors[0].location === 'query' || err.errors[0].location === 'body') {
        return next(new apiError(422, 0, err.message, null, err.errors));
      }
    }
    if (err instanceof httpErrors.MethodNotAllowed) {
      return next(new apiError(405, 0, err.message));
    }
    return next(new apiError(500, 0, 'Unexpect error!', err));
  }
  next(err);
});

// Handle ApiError
app.use((err, req, res, next) => {
  const errors = {
    message: err.message,
    code: err.code,
    errors: err.errors,
    debug: config.env === 'production' ? {} : err.getStacks()
  };
  res.status(err.status).json(errors);
  console.log({status:err.status , errors: errors, request: {route: req.url, body: req.body}});
  next();
});

//app.listen(config.port);
module.exports = app;