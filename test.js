// https://docs.aws.amazon.com/zh_tw/amazondynamodb/latest/developerguide/GettingStarted.NodeJs.03.html

var AWS = require("aws-sdk");
var fs = require("fs");

AWS.config.update({
  region: "localhost1",
  endpoint: "http://localhost:8000"
});

function loadExample() {
  var docClient = new AWS.DynamoDB.DocumentClient();

  console.log("Importing movies into DynamoDB. Please wait.");

  var allMovies = JSON.parse(fs.readFileSync("moviedata.json", "utf8"));
  console.log(allMovies);
  allMovies.forEach(function(movie) {
    var params = {
      TableName: "Movies",
      Item: {
        year: movie.year,
        title: movie.title,
        info: movie.info
      }
    };

    docClient.put(params, function(err, data) {
      if (err) {
        console.error(
          "Unable to add movie",
          movie.title,
          ". Error JSON:",
          JSON.stringify(err, null, 2)
        );
      } else {
        console.log("PutItem succeeded:", movie.title);
      }
    });
  });
}

function addItem() {
  var docClient = new AWS.DynamoDB.DocumentClient();

  var table = "Movies";

  var year = 2015;
  var title = "The Big New Movie";

  var params = {
    TableName: table,
    Item: {
      year: year,
      title: title,
      info: {
        plot: "Nothing happens at all.",
        rating: 0
      }
    }
  };

  console.log("Adding a new item...");
  docClient.put(params, function(err, data) {
    if (err) {
      console.error(
        "Unable to add item. Error JSON:",
        JSON.stringify(err, null, 2)
      );
    } else {
      console.log("Added item:", JSON.stringify(data, null, 2));
    }
  });
}

function getItem() {
  var docClient = new AWS.DynamoDB.DocumentClient();

  var table = "Movies";

  var year = 2015;
  var title = "The Big New Movie";

  var params = {
    TableName: table,
    Key: {
      year: year,
      title: title
    }
  };

  docClient.get(params, function(err, data) {
    if (err) {
      console.error(
        "Unable to read item. Error JSON:",
        JSON.stringify(err, null, 2)
      );
    } else {
      console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
    }
  });
}

function updateItem() {
  var docClient = new AWS.DynamoDB.DocumentClient();

  var table = "Movies";

  var year = 2015;
  var title = "The Big New Movie";

  // Update the item, unconditionally,

  var params = {
    TableName: table,
    Key: {
      year: year,
      title: title
    },
    UpdateExpression: "set info.rating = :r, info.plot=:p, info.actors=:a",
    ExpressionAttributeValues: {
      ":r": 5.5,
      ":p": "Everything happens all at once.",
      ":a": ["Larry", "Moe", "Curly"]
    },
    ReturnValues: "UPDATED_NEW"
  };

  console.log("Updating the item...");
  docClient.update(params, function(err, data) {
    if (err) {
      console.error(
        "Unable to update item. Error JSON:",
        JSON.stringify(err, null, 2)
      );
    } else {
      console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
    }
  });
}

function query() {
  var docClient = new AWS.DynamoDB.DocumentClient();

  console.log("Querying for movies from 2013.");

  var params = {
    TableName: "Movies",
    KeyConditionExpression: "#yr = :yyyy",
    ExpressionAttributeNames: {
      "#yr": "year"
    },
    ExpressionAttributeValues: {
      ":yyyy": 2013
    }
  };

  docClient.query(params, function(err, data) {
    if (err) {
      console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
    } else {
      console.log("Query succeeded.");
      data.Items.forEach(function(item) {
        console.log(" -", item.year + ": " + item.title);
      });
    }
  });
}

function listTables() {
  var ddb = new AWS.DynamoDB({ apiVersion: "2012-08-10" });
  // Call DynamoDB to retrieve the list of tables
  ddb.listTables({ Limit: 10 }, function(err, data) {
    if (err) {
      console.log("Error", err.code);
    } else {
      console.log("Table names are ", data.TableNames);
    }
  });
}

function createTable(tableName) {
  var dynamodb = new AWS.DynamoDB();

  var params = {
    TableName: tableName,
    KeySchema: [
      { AttributeName: "year", KeyType: "HASH" }, //Partition key
      { AttributeName: "title", KeyType: "RANGE" } //Sort key
    ],
    AttributeDefinitions: [
      { AttributeName: "year", AttributeType: "N" },
      { AttributeName: "title", AttributeType: "S" }
    ],
    ProvisionedThroughput: {
      ReadCapacityUnits: 10,
      WriteCapacityUnits: 10
    }
  };

  dynamodb.createTable(params, function(err, data) {
    if (err) {
      console.error(
        "Unable to create table. Error JSON:",
        JSON.stringify(err, null, 2)
      );
    } else {
      console.log(
        "Created table. Table description JSON:",
        JSON.stringify(data, null, 2)
      );
    }
  });
}

// createTable("Movies2");
// loadExample();
// addItem();
// getItem();
// updateItem();

// query();
listTables();
