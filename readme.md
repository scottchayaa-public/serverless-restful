
# Deploy Serverless to AWS
```sh
# list configure
aws configure list

# Remove Serverless service and all resources (optional))
serverless remove

# Deploy a Serverless service
serverless deploy -v

# Test lambda function with params
serverless invoke -f [function] -d '{"key3": "value3","key2": "value2","key1": "value1"}'

```

# Serverless Monitoring
```sh
# Print logs from cloudwatch
serverless logs -f [function] 
serverless logs -f [function] -t  # Tail the log output

# List deployed version of your Serverless Service 
serverless deploy list 
```

# Run Serverless on Localhost

```sh
# Run serverless on localhost
serverless offline start --noTimeout  # default: .env.development, if not exist then .env
serverless offline start --env __env_filename__
```

> It will also launch `dynamodb-local`.


# Use Dynamodb-admin for dynamodb-local

[dynamodb-admin](https://github.com/aaronshaf/dynamodb-admin) : It's a localhost dynamodb viewer.

```sh
npm install dynamodb-admin -g
export DYNAMO_ENDPOINT=http://localhost:8000

# default will run on :8001
dynamodb-admin
```

# References
 - [example: icarus/serverless.yml](https://github.com/buildit/icarus/blob/master/serverless.yml)
 - [Serverless DynamoDB Local](https://serverless.com/plugins/serverless-dynamodb-local/)