
class ApiError extends Error {
  constructor(status, code = 0, message = null, previous = null, errors = [], headers = []) {
    super(message);
    this.name = this.constructor.name;
    this.status = status;
    this.code = code;
    this.message = message;
    this.errors = errors;
    this.headers = headers;
    this.previous = previous;
  }

  getStacks() {
    let stacks = [this.stack.split("\n")];
    let previous = this.previous;
    
    while (previous) {
      stacks.push((previous instanceof Error)?previous.stack.split("\n"):previous);
      previous = previous.previous;
    }
    return stacks;
  }
}

module.exports = ApiError;