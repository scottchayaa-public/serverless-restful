const uuid = require("uuid");
const AWS = require("aws-sdk");

const apiError = require("../api-error");
const config = require("../config");

// const dynamoDb = require("../dynamodb");
// const dynamoDb = new AWS.DynamoDB.DocumentClient();

exports.list = async (req, res) => {

  AWS.config.update({
    region: "us-east-1",
    endpoint: "http://localhost:8000"
  });

  var dynamodb = new AWS.DynamoDB();

  var params = {
    TableName: "Movies",
    KeySchema: [
      { AttributeName: "year", KeyType: "HASH" }, //Partition key
      { AttributeName: "title", KeyType: "RANGE" } //Sort key
    ],
    AttributeDefinitions: [
      { AttributeName: "year", AttributeType: "N" },
      { AttributeName: "title", AttributeType: "S" }
    ],
    ProvisionedThroughput: {
      ReadCapacityUnits: 10,
      WriteCapacityUnits: 10
    }
  };

  dynamodb.createTable(params, function(err, data) {
    if (err) {
      console.error(
        "Unable to create table. Error JSON:",
        JSON.stringify(err, null, 2)
      );
    } else {
      console.log(
        "Created table. Table description JSON:",
        JSON.stringify(data, null, 2)
      );
    }
  });
  // let data;
  // try {
  //   data = await dynamoDb.createTable(params).promise();
  // } catch (error) {
  //   throw new apiError(500, 0, error);
  // }

  // console.log(data);
  // process.exit(1);
  res.status(200).send("aaa");
};

exports.create = async (req, res) => {
  const timestamp = new Date().getTime();

  const params = {
    TableName: config.table.users,
    Item: {
      id: req.body.id,
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
      createdAt: timestamp,
      updatedAt: timestamp
    }
  };

  try {
    await dynamoDb.put(params).promise();
  } catch (error) {
    throw new apiError(501, 0, "Cannot create the user data.", error);
  }

  res.status(201).send(params.Item);
  // res.status(201).send({message: "Hello"});
};

exports.update = async (req, res) => {};

exports.delete = async (req, res) => {};

exports.login = async (req, res) => {
  const params = {
    TableName: config.table.users,
    Key: {
      email: req.body.email
    }
  };

  let data;
  try {
    data = await dynamoDb.get(params).promise();
  } catch (error) {
    throw new apiError(501, 0, "Cannot get the user data.", error);
  }

  if (!data.Item) {
    throw new apiError(401, 0, "no email data");
  }

  if (data.Item.password != req.body.password) {
    throw new apiError(401, 0, "password error");
  }

  res.status(200).send(data.Item);
};

exports.logout = async (req, res) => {};
