'use strict';

const joi = require('joi');

module.exports.create = {
  body: {
    name: joi.string().required(),
    email: joi.string().email().required(),
    password: joi.string().min(5).required()
  }
};

module.exports.update = {
  body: {
    name: joi.string(),
    email: joi.string().email(),
    password: joi.string().min(5)
  }
};

module.exports.login = {
  body: {
    email: joi.string().email().required(),
    password: joi.string().min(5).required()
  }
};