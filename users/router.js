const express = require('express');
require('express-async-errors');
const router = express.Router();
const validate = require('express-validation');
const allowMethods = require('allow-methods');

const validation = require('./validation');
const controller = require('./controller');

router.route('/')
  .get(controller.list)
  .post(validate(validation.create), controller.create)
  .all(allowMethods(['get', 'post']));

router.route('/:id(^\\d+$)')
  .put(validate(validation.update), controller.update)
  .delete(controller.delete)
  .all(allowMethods(['put', 'delete']));

router.route('/login').post(validate(validation.login), controller.login).all(allowMethods(['post']));
router.route('/logout').get(controller.logout).all(allowMethods(['get']));

module.exports = router;