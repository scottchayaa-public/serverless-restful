const express = require("express");
require("express-async-errors");
const router = express.Router();

const apiError = require("./api-error");
const config = require("./config");

router.route("/").get(async (req, res) => {
  res.json({
    msg: `Hello ${config.service}`
  });
});

router.route("/demo-errors-1").get(async (req, res) => {
  throw new apiError(400, 0, "error03");
});

router.route("/demo-errors-2").get(async (req, res) => {
  var error01 = { error: "aaa", vvvv: "dwdwdwd" };
  var error02 = new apiError(400, 0, "error02", error01);
  var error03 = new apiError(400, 0, "error03", error02);
  throw error03;
});

router.route("/config").get(async (req, res) => {
  res.json(config);
});

router.route("/env").get(async (req, res) => {
  res.json(process.env);
});

module.exports = router;
